from window.ui import widgets
from vendor.Qt import QtCore, QtGui, QtWidgets


class BrowserPane(widgets.BaseWidget):
    def __init__(self, parent=None):
        super(BrowserPane, self).__init__(parent)

        self.control = BrowserControl()

        self.tree = BrowserTree()
        self.tree.setFixedWidth(200)
        self.grid = BrowserGrid()
        self.grid.set_scale(self.control.scale())

        h_layout = QtWidgets.QHBoxLayout()
        h_layout.addWidget(self.tree)
        h_layout.addWidget(self.grid)

        layout = QtWidgets.QVBoxLayout(self)
        layout.addWidget(self.control)
        layout.addLayout(h_layout)

        self.tree.currentItemChanged.connect(self.__populate_item_handle)
        self.control.filter_changed.connect(self.__populate_item_handle)
        self.control.scale_changed.connect(self.grid.set_scale)

    # private methods
    def __populate_item_handle(self, *args):
        item = self.tree.currentItem()
        if item:
            self.grid.populate(item.entity)


class BrowserTreeItem(QtWidgets.QTreeWidgetItem):
    def __init__(self, name, entity, parent=None):
        super(BrowserTreeItem, self).__init__(parent)

        self.entity = entity

        self.setText(0, name)


class BrowserTree(widgets.BaseTreeWidget):
    def __init__(self, parent=None):
        super(BrowserTree, self).__init__(parent)

        self.headerItem().setText(0, 'Name')

        self.setSortingEnabled(True)
        self.sortItems(0, QtCore.Qt.AscendingOrder)

        self.populate()

    def populate(self):
        temp = [0]

        def _id(x):
            x[0] += 1
            return x[0]

        for name, entity in (
                ("Movie", {
                    "id": _id(temp),
                    "tasks": ["Modeling"]
                    }
                 ),
                ("Assets", {
                    "id": _id(temp),
                    "tasks": ["Modeling", "Rigging"]
                    }
                 )
        ):
            widget = BrowserTreeItem(name, entity, self)
            for item in ("one", "two", "three"):
                BrowserTreeItem(item, {
                    "id": _id(temp),
                    "tasks": ["FX", "Animation", "Layout", "Lighting"]
                }, widget)


class BrowserControl(widgets.BaseWidget):
    filter_changed = QtCore.Signal(list)
    scale_changed = QtCore.Signal(float)

    def __init__(self, parent=None):
        super(BrowserControl, self).__init__(parent)

        self.__project = widgets.BasePushButton("PFX_ANIM")
        self.__project.setFixedWidth(200)
        self.__filters = list()

        layout = QtWidgets.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__project)

        self.__slider = widgets.FieldSlider(precision=1)
        self.__slider.setMaximumWidth(200)
        self.__slider.setMinimum(0.1)
        self.__slider.setMaximum(2.0)
        self.__slider.setValue(1.0)
        self.__slider.spinbox().setFixedWidth(40)
        layout.addWidget(self.__slider)

        filters_layout = QtWidgets.QHBoxLayout()
        filters_layout.setContentsMargins(0, 0, 0, 0)
        filters_layout.setSpacing(2)

        filters = {
            "Lighting": True,
            "FX": True,
            "Animation": True,
            "Rigging": True,
            "Modeling": True,
            "Generic": False,
            "Layout": True
        }
        for label, value in sorted(filters.items()):
            button = widgets.FilterToggledButton(label, value)
            button.toggled.connect(self.__filter_toggled_handle)
            self.__filters.append(button)
            filters_layout.addWidget(button)
        layout.addLayout(filters_layout)
        layout.addStretch()

        self.__slider.value_changed.connect(self.scale_changed.emit)

    # private methods
    def __filter_toggled_handle(self, *args):
        self.filter_changed.emit(self.filters())

    # query methods
    def scale(self):
        return self.__slider.value()

    def filters(self):
        return [x.text() for x in self.__filters if x.isChecked()]


class BrowserGrid(QtWidgets.QScrollArea):
    def __init__(self, parent=None):
        super(BrowserGrid, self).__init__(parent)

        self.__selection = None

        self.__scale = 1.0
        self.__widgets = dict()
        self.__filtered = list()

        self.setWidgetResizable(True)
        self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.setFrameShadow(QtWidgets.QFrame.Plain)

        inner = widgets.InnerFrame(self)
        inner.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self._layout = QtWidgets.QGridLayout()
        self._layout.setContentsMargins(2, 2, 2, 2)
        self._layout.setSpacing(6)
        inner.setLayout(self._layout)
        self.setWidget(inner)

        self.__menu = BrowserGridPopUp(self)

        # setting handlers
        inner.customContextMenuRequested.connect(self.__context_menu_requested)

    # private methods
    def __update_placement(self):
        cell_width, cell_height = self.cellSize()
        stretch_col = int(self.width() / (self._layout.spacing() + cell_width))
        self._layout.setColumnStretch(stretch_col, 1)
        row = 0
        col = 0
        if self.__filtered:
            for widget in self.__filtered:
                widget.setFixedSize(cell_width, cell_height)
                if col == stretch_col:
                    row += 1
                    col = 0
                self._layout.addWidget(widget, row, col)
                col += 1
            self._layout.setRowStretch(row + 1, 1)

    def __preview_button_released_handle(self, widget, event):
        if event.button() == QtCore.Qt.LeftButton:
            if self.__selection == widget:
                widget.setSelected(False)
                self.__selection = None
            else:
                self.clearSelection()
                widget.setSelected(True)
                self.__selection = widget

    def __context_menu_requested(self, pos):
        widget = self.widget().childAt(pos)
        if widget:
            self.__menu.exec_(self.widget().mapToGlobal(pos))

    # events
    def resizeEvent(self, event):
        super(BrowserGrid, self).resizeEvent(event)
        self.__update_placement()

    def mouseReleaseEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.clearSelection()

    # query methods
    def cellSize(self):
        return int(160 * self.__scale), int(120 * self.__scale)

    # setting methods
    def clear(self):
        for widget in self.__filtered:
            widget.setVisible(False)
        self.__filtered = list()

    def clearSelection(self):
        if self.__selection:
            self.__selection.setSelected(False)
        self.__selection = None

    def set_scale(self, value):
        self.__scale = float(value)
        self.__update_placement()

    def populate(self, entity):
        self.clear()
        filters = self.parent().control.filters()
        if filters:
            for task in entity.get("tasks", list()):
                if task in filters:
                    id_ = str(entity.get("id")) + " / " + task
                    widget = self.__widgets.get(id_)
                    if not widget:
                        widget = widgets.PreviewItem(id_, self.widget())
                        widget.button_released.connect(self.__preview_button_released_handle)
                        self.__widgets[id_] = widget
                    widget.show()
                    self.__filtered.append(widget)
            self.__update_placement()


class BrowserGridPopUp(QtWidgets.QMenu):
    def __init__(self, parent=None):
        super(BrowserGridPopUp, self).__init__(parent)

        self.addAction(
            QtWidgets.QAction('Revive Version', self, triggered=self.__revive_handle)
        )
        self.addAction(
            QtWidgets.QAction('Submit to Deadline', self, triggered=self.__submit_to_deadline_handle)
        )

    def __revive_handle(self):
        print("Revive")

    def __submit_to_deadline_handle(self):
        print("Submit")
