# noinspection PyUnresolvedReferences
from vendor.Qt import QtCore, QtGui, QtWidgets
import window.ui.palette


temp = {
    "scene":
        [
            ('bool', "check"),
            ('integer', "padding"),
            ('option', ""),
            ('float', "value")
        ],
    "alembic":
        [
            ('integer', "start"),
            ('integer', "end"),
        ]
}


def get_palette(definition, palette=QtGui.QPalette()):
    for color_role, role_data in definition.items():
        for color_group, color in role_data.items():
            palette.setColor(
                getattr(QtGui.QPalette.ColorGroup, color_group),
                getattr(QtGui.QPalette.ColorRole, color_role),
                QtGui.QColor(*color)
            )
    return palette


def get_color_group(option):
    if QtWidgets.QStyle.State_Enabled & option.state:
        if QtWidgets.QStyle.State_Active & option.state:
            return QtGui.QPalette.ColorGroup.Active
        else:
            return QtGui.QPalette.ColorGroup.Inactive
    else:
        return QtGui.QPalette.ColorGroup.Disabled


class BaseWidget(QtWidgets.QWidget):
    def __init__(self, *args, **kwargs):
        super(BaseWidget, self).__init__(*args, **kwargs)

    def paintEvent(self, event):
        palette = self.palette()
        bg_color = palette.color(QtGui.QPalette.ColorRole.Window)
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.fillRect(self.rect(), bg_color)
        qp.end()


class BaseFrame(QtWidgets.QFrame):
    def __init__(self, *args, **kwargs):
        super(BaseFrame, self).__init__(*args, **kwargs)

    def paintEvent(self, event):
        option = QtWidgets.QStyleOptionFrame()
        self.initStyleOption(option)
        palette = self.palette()
        color_group = get_color_group(option)
        bg_color = palette.color(color_group, QtGui.QPalette.ColorRole.Window)
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.fillRect(self.rect(), bg_color)
        qp.end()


class InnerFrame(BaseFrame):
    def __init__(self, *args, **kwargs):
        super(InnerFrame, self).__init__(*args, **kwargs)

        definition = {
            "Window": window.ui.palette.definition.get("Base"),
        }
        self.setPalette(
            get_palette(definition, self.palette())
        )


class BasePushButton(QtWidgets.QPushButton):
    def __init__(self, *args, **kwargs):
        super(BasePushButton, self).__init__(*args, **kwargs)

    def paintEvent(self, event):
        option = QtWidgets.QStyleOptionButton()
        self.initStyleOption(option)
        palette = self.palette()
        color_group = get_color_group(option)
        bg_darker = 100
        bg_lighter = 100
        fg_darker = 100
        fg_lighter = 100
        if not self.isCheckable() or self.isChecked():
            bg_color = palette.color(color_group, QtGui.QPalette.ColorRole.Button)
        else:
            bg_color = palette.color(color_group, QtGui.QPalette.ColorRole.Background)
            fg_darker += 60
        # define hover
        if QtWidgets.QStyle.State_MouseOver & option.state:
            fg_color = palette.color(color_group, QtGui.QPalette.ColorRole.HighlightedText)
            bg_lighter += 10
        else:
            fg_color = palette.color(color_group, QtGui.QPalette.ColorRole.ButtonText)
        # define pressed
        if QtWidgets.QStyle.State_Sunken & option.state:
            bg_darker += 20
            fg_darker += 20
        # apply color corrections
        if bg_darker > 100:
            bg_color = bg_color.darker(bg_darker)
        if bg_lighter > 100:
            bg_color = bg_color.lighter(bg_lighter)
        if fg_darker > 100:
            fg_color = fg_color.darker(fg_darker)
        if fg_lighter > 100:
            fg_color = fg_color.lighter(fg_lighter)
        # draw control
        bd_color = bg_color.lighter(120)
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing, True)
        qp.fillRect(self.rect(), bg_color)
        pen = QtGui.QPen(bd_color)
        pen.setWidth(2)
        qp.setPen(pen)
        qp.drawRect(self.rect())
        qp.setPen(QtGui.QPen(fg_color))
        qp.drawText(self.rect(), QtCore.Qt.AlignCenter, self.text())
        qp.end()


class BaseToggledButton(BasePushButton):
    def __init__(self, label, value=False, parent=None):
        super(BaseToggledButton, self).__init__(parent)

        self.__margin = 6

        self.set_label(label)
        self.setCheckable(True)
        self.setChecked(value)

    def set_label(self, label):
        self.setText(label)
        fm = QtGui.QFontMetrics(self.font())
        text_width = fm.width(label)
        self.setFixedWidth(text_width + 2 * self.__margin)


class FilterToggledButton(BaseToggledButton):
    def __init__(self, label, value=False, parent=None):
        super(FilterToggledButton, self).__init__(label, value, parent)

        definition = {
            "Button": {
                "Active": (82, 133, 166, 255),
                "Disabled": (75, 75, 75, 255),
                "Inactive": (82, 133, 166, 255)
            }
        }
        self.setPalette(
            get_palette(definition, self.palette())
        )


class BaseHeaderView(QtWidgets.QHeaderView):
    def __init__(self, *args, **kwargs):
        super(BaseHeaderView, self).__init__(*args, **kwargs)

        self.__margin = 6
        self.__align = QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter
        self.__elide = QtCore.Qt.ElideRight

        self.setStretchLastSection(True)

    def paintSection(self, qp, rect, index):
        option = QtWidgets.QStyleOptionHeader()
        self.initStyleOption(option)
        palette = self.palette()
        color_group = get_color_group(option)

        text = self.model().headerData(index, self.orientation(), QtCore.Qt.DisplayRole)

        pen = QtGui.QPen(QtGui.QColor(110, 110, 110))
        pen.setWidth(2)
        qp.setPen(pen)
        qp.drawRect(rect)

        fg_color = palette.color(color_group, QtGui.QPalette.ColorRole.ButtonText)
        qp.setPen(QtGui.QPen(fg_color))

        fm = QtGui.QFontMetrics(self.font())
        qp.drawText(
            QtCore.QRect(
                rect.x() + self.__margin,
                rect.y(),
                rect.width() - 2 * self.__margin,
                rect.height()
            ),
            self.__align,
            fm.elidedText(text, self.__elide, rect.width())
        )


class BaseTreeWidget(QtWidgets.QTreeWidget):
    def __init__(self, *args, **kwargs):
        super(BaseTreeWidget, self).__init__(*args, **kwargs)

        self.setHeader(BaseHeaderView(QtCore.Qt.Horizontal))


class HorizontalLine(QtWidgets.QFrame):
    def __init__(self, parent=None):
        super(HorizontalLine, self).__init__(parent)

        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)


class BaseSplitterSpacer(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(BaseSplitterSpacer, self).__init__(parent)


class BaseSplitterHandle(QtWidgets.QSplitterHandle):
    def __init__(self, orientation, parent=None):
        super(BaseSplitterHandle, self).__init__(orientation, parent)

    # events
    def paintEvent(self, event):
        parent = self.parent()
        dummy = True
        for index in reversed(range(parent.count())):
            widget = parent.widget(index)
            if isinstance(widget, BaseSplitterSpacer):
                pass
            elif isinstance(widget, BaseControl) and not widget.contentVisible():
                pass
            elif isinstance(widget, QtWidgets.QLabel) and not widget.text():
                pass
            else:
                dummy = False
                break
            if parent.handle(index) == self:
                break
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing, True)
        qp.fillRect(self.rect(), QtGui.QColor(100, 100, 200) if dummy else QtGui.QColor(68, 68, 68))
        qp.end()


class BaseSplitter(QtWidgets.QSplitter):
    state_changed = QtCore.Signal(object, int, int)

    def __init__(self, parent=None):
        super(BaseSplitter, self).__init__(parent)

        self.__limit = 60

        self.setChildrenCollapsible(False)

        self.splitterMoved.connect(self.state_changed_handle)

        self.addWidget(BaseSplitterSpacer())

    # setting methods
    def createHandle(self):
        return BaseSplitterHandle(self.orientation(), self)

    def addWidget(self, widget):
        count = self.count()
        self.insertWidget(count - 1 if count > 0 else 0, widget)

    def insertWidget(self, index, widget):
        # toDO: all columns should have the same limits
        count = self.count()
        if count > 0:
            if widget.minimumWidth() < self.__limit:
                widget.setMinimumWidth(self.__limit)
        super(BaseSplitter, self).insertWidget(index, widget)

    def state_changed_handle(self, pos, index):
        self.state_changed.emit(self, pos, index)


class BaseControlSplitter(BaseSplitter):
    expander_toggled = QtCore.Signal(bool)
    checker_toggled = QtCore.Signal(bool)

    def __init__(self, name=None, annotation=None, parent=None):
        super(BaseControlSplitter, self).__init__(parent)

        self.__controls = True

        self.addWidget(ControlExpander(name))
        self.addWidget(ElidedLabel(annotation))

        self.setFixedHeight(24)

        self.widget(0).control().expander.toggled.connect(self.expander_toggled.emit)
        self.widget(0).control().checker.toggled.connect(self.checker_toggled.emit)

    # query methods
    def name(self):
        return self.widget(0).text()

    def annotation(self):
        return self.widget(1).text()

    def controlsVisible(self):
        return self.__controls

    # setting methods
    def insertWidget(self, index, widget):
        if isinstance(widget, BaseControl):
            widget.setContentVisible(self.__controls)
        super(BaseControlSplitter, self).insertWidget(index, widget)

    def setName(self, name):
        self.widget(0).setText(name)

    def setAnnotation(self, annotation):
        self.widget(1).setText(annotation)

    def setControlsVisible(self, value):
        count = self.count()
        if count > 3:
            for index in range(3, count):
                control = self.widget(index)
                if isinstance(control, BaseControl):
                    control.setContentVisible(value)
        self.update()
        self.__controls = value


class ProcessSplitter(BaseControlSplitter):
    def __init__(self, name=None, annotation=None, parent=None):
        super(ProcessSplitter, self).__init__(name, annotation, parent)

        self.widget(0).setMargins(30, 2, 2, 2)

        self.addWidget(BaseSplitterSpacer())

        self.setStyleSheet("QSplitter{background-color: rgb(128, 128, 200)}")


class FamilySplitter(BaseControlSplitter):
    def __init__(self, name=None, annotation=None, parent=None):
        super(FamilySplitter, self).__init__(name, annotation, parent)

        font = self.widget(0).textFont()
        font.setWeight(QtGui.QFont.Bold)
        self.widget(0).setTextFont(font)
        self.widget(0).setMargins(8, 2, 2, 2)

        self.addWidget(ControlCheckBox("family settings"))

        self.setStyleSheet("QSplitter{background-color: rgb(200, 128, 128)}")


class BaseExpandedContainer(QtWidgets.QWidget):
    widget_added = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(BaseExpandedContainer, self).__init__(parent)

        self._widgets = list()

        # Widget
        self._layout = QtWidgets.QVBoxLayout(self)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._layout.setSpacing(2)

    # private methods
    def __expander_toggled_handle(self, value):
        extend = QtWidgets.QApplication.keyboardModifiers() == QtCore.Qt.ShiftModifier
        for widget in self._widgets[1:]:
            if extend and isinstance(widget, BaseExpandedContainer):
                widget.setExpanded(value)
            widget.setVisible(value)

    def __checker_toggled_handle(self, value):
        extend = QtWidgets.QApplication.keyboardModifiers() == QtCore.Qt.ShiftModifier
        if extend:
            for widget in self._widgets[1:]:
                if isinstance(widget, BaseExpandedContainer):
                    widget = widget.widget(0)
                    if widget:
                        widget = widget.widget(0)
                        if isinstance(widget, ControlExpander):
                            widget.setCheckerValue(value)

    # query methods
    def widgets(self):
        return self._widgets

    def widget(self, index):
        if index < len(self._widgets):
            return self._widgets[index]
        return None

    def expanded(self):
        if self._widgets:
            return self._widgets[0].widget(0).expanderValue()
        return None

    # setting methods
    def add_widget(self, widget):
        if self._widgets:
            self._widgets[0].widget(0).setExpanderVisible(True)
            widget.setVisible(self._widgets[0].widget(0).expanderValue())
        else:
            if not isinstance(widget, BaseControlSplitter):
                raise RuntimeError("Invalid object type!")
            widget.expander_toggled.connect(self.__expander_toggled_handle)
            widget.checker_toggled.connect(self.__checker_toggled_handle)
            widget.widget(0).setExpanderVisible(False)
        self._layout.addWidget(widget)
        self._widgets.append(widget)

        self.widget_added.emit(widget)

    def setExpanded(self, value, force=True):
        if self._widgets:
            if value != self._widgets[0].widget(0).expanderValue():
                self._widgets[0].widget(0).setExpanderValue(value)
            elif force:
                self.__expander_toggled_handle(value)


class ProcessContainer(BaseExpandedContainer):
    def __init__(self, parent=None):
        super(ProcessContainer, self).__init__(parent)

    def add(self, option, message):
        self.add_widget(StatusLine(option, message))


class FamilyContainer(BaseExpandedContainer):
    state_changed = QtCore.Signal(object, int, int)

    def __init__(self, parent=None):
        super(FamilyContainer, self).__init__(parent)

    # private methods
    def __family_settings_toggled_handle(self, value):
        for widget in self._widgets:
            splitter = widget.widget(0) if isinstance(widget, BaseExpandedContainer) else widget
            if isinstance(splitter, FamilySplitter):
                state = value
            else:
                state = not value
            splitter.setControlsVisible(state)

    # query methods
    def familySettings(self):
        if self._widgets:
            return self._widgets[0].widget(2).value()
        return None

    # setting methods
    def add(self, name, annotation=None):
        if self._widgets:
            temp_fam = self._widgets[0]
            widget = ProcessContainer()
            splitter = ProcessSplitter(name, annotation)
            widget.add_widget(splitter)
            splitter.setControlsVisible(not self.familySettings())
        else:
            widget = splitter = temp_fam = FamilySplitter(name, annotation)
            splitter.widget(2).control().toggled.connect(self.__family_settings_toggled_handle)

        # toDo
        controls = temp.get(temp_fam.name())
        if controls:
            classes_ = {
                'bool': ControlCheckBox,
                'integer': ControlSpinBox,
                'option': ControlComboBox,
                'float': ControlSpinBox
            }

            for type_, label_ in controls:
                splitter.addWidget(classes_[type_](label_))

        self.add_widget(widget)

        return widget

    def setFamilySettings(self, value, force=False):
        if self._widgets:
            if value != self.familySettings():
                self._widgets[0].widget(2).setValue(value)
            elif force:
                self.__family_settings_toggled_handle(value)


class Status(QtWidgets.QWidget):
    Error = 0
    Warning = 1
    Info = 2

    def __init__(self, option, parent=None):
        super(Status, self).__init__(parent)
        self.__option = option
        self.setFixedSize(16, 16)

    # events
    def paintEvent(self, event):
        if self.__option == Status.Info:
            color = 40, 180, 80
        elif self.__option == Status.Error:
            color = 240, 30, 30
        elif self.__option == Status.Warning:
            color = 250, 200, 15
        else:
            return
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.fillRect(self.rect(), QtGui.QColor(*color))
        qp.end()


class StatusLine(QtWidgets.QWidget):
    def __init__(self, option, message, parent=None):
        super(StatusLine, self).__init__(parent)

        self._widgets = list()

        # Widget
        self._layout = QtWidgets.QHBoxLayout(self)
        self._layout.setContentsMargins(45, 0, 0, 0)
        self._layout.setSpacing(6)

        self._layout.addWidget(Status(option))
        label = ElidedLabel(message)
        label.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        self._layout.addWidget(label)


class ElidedLabel(QtWidgets.QLabel):
    def __init__(self, text=None, parent=None):
        super(ElidedLabel, self).__init__(parent)

        self.__align = QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft
        self.__elide = QtCore.Qt.ElideRight

        self.setText(text)
        self.setMinimumWidth(1)  # if 0 - widget never gets its minimum

    # events
    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing, True)
        fm = QtGui.QFontMetrics(self.font())
        palette = self.palette()
        option = QtWidgets.QStyleOptionFrame()
        self.initStyleOption(option)
        color_group = get_color_group(option)
        pen = QtGui.QPen()
        pen.setColor(palette.color(color_group, QtGui.QPalette.Text))
        qp.setPen(pen)
        selected = self.selectedText()
        if not selected:
            qp.drawText(
                self.rect(),
                self.__align,
                fm.elidedText(self.text(), self.__elide, self.width())
            )
        else:
            whole_text = self.text()
            before = whole_text[:self.selectionStart()]
            after = whole_text[self.selectionStart() + len(selected):]
            value = fm.width(before)
            if self.width() <= value:
                spans = [self.width(), 0, 0]
            else:
                spans = [value]
                rest = self.width() - value
                value = fm.width(selected)
                if rest <= value:
                    spans.extend([rest, 0])
                else:
                    spans.append(value)
                    spans.append(min(rest - value, fm.width(after)))
            qp.drawText(QtCore.QRect(0, 0, spans[0], self.height()), before)
            qp.drawText(
                QtCore.QRect(spans[0] + spans[1], 0, spans[2], self.height()),
                self.__align,
                fm.elidedText(after, self.__elide, spans[2])
            )
            selection_rect = QtCore.QRect(spans[0], 0, spans[1], self.height())
            qp.fillRect(selection_rect, palette.color(color_group, QtGui.QPalette.Highlight))
            pen = QtGui.QPen()
            pen.setColor(palette.color(color_group, QtGui.QPalette.HighlightedText))
            qp.setPen(pen)
            qp.drawText(selection_rect, selected)
        qp.end()

    # query methods
    def alignment(self):
        return self.__align

    def elision(self):
        return self.__elide

    # setting methods
    def setText(self, text):
        super(ElidedLabel, self).setText(text or "")

    def setAlignment(self, alignment):
        self.__align = alignment

    def setElision(self, elision):
        self.__elide = elision


class ArrowCheckBox(QtWidgets.QCheckBox):
    def __init__(self, color=(189, 189, 189), *args, **kwargs):
        super(ArrowCheckBox, self).__init__(*args, **kwargs)

        self.color = color
        self.setFixedSize(9, 9)

        self.__renderable = True

    # events
    def paintEvent(self, event):
        if self.__renderable:
            painter = QtGui.QPainter(self)

            painter.setBrush(QtGui.QColor(*self.color))

            if self.isChecked():
                points = (
                    QtCore.QPointF(0.0, 2.0),
                    QtCore.QPointF(8.0, 2.0),
                    QtCore.QPointF(4.0, 6.0))
            else:
                points = (
                    QtCore.QPointF(2.0, 0.0),
                    QtCore.QPointF(6.0, 4.0),
                    QtCore.QPointF(2.0, 8.0))

            painter.drawPolygon(points)

            painter.end()

    # query methods
    def renderable(self):
        return self.__renderable

    # setting methods
    def setRenderable(self, value):
        self.__renderable = value


class BaseControl(QtWidgets.QWidget):
    AnchorLeft = 0
    AnchorRight = 1
    LabelLeft = 0
    LabelRight = 1

    def __init__(self, label=None, parent=None):
        super(BaseControl, self).__init__(parent)

        # UI settings
        self.__content_visible = True
        self.__anchor = None
        self.__label_place = None
        self.__spacing = 6
        self.__margins = (2, 2, 2, 2)

        self.__label_width_constraint = 100
        self.__control_width_range = 50, 120
        self.__control_height_range = None, None

        self._control = self.createControl()
        self._control.setParent(self)
        self._label = ElidedLabel()
        self._label.setParent(self)
        self.setText(label)
        self.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
        self.setTextElision(QtCore.Qt.ElideRight)

    # events
    def showEvent(self, event):
        self._control.show()
        self._label.show()
        # toDO: resize problem for checkbox
        if not self.__content_visible:
            self._control.hide()
            self._label.hide()

    def hideEvent(self, event):
        self._control.hide()
        self._label.hide()

    def paintEvent(self, event):
        if self.text():
            spacing = self.__spacing
            fm = QtGui.QFontMetrics(self._label.font())
            text_width = self.__label_width_constraint or fm.width(self.text())
        else:
            spacing = text_width = 0

        control_policy = self.controlSizePolicy()
        if control_policy.horizontalPolicy() == QtWidgets.QSizePolicy.Fixed:
            control_width = self._control.width()
        else:
            control_width = self.width() - text_width - spacing - self.__margins[0] - self.__margins[2]
            if self.controlMaxWidth() is not None:
                control_width = min(control_width, self.controlMaxWidth())
            control_width = max(control_width, self.controlMinWidth())
        if control_policy.verticalPolicy() == QtWidgets.QSizePolicy.Fixed:
            control_height = self._control.height()
        else:
            control_height = self.height() - self.__margins[1] - self.__margins[3]
            if self.controlMaxHeight() is not None:
                control_height = min(control_height, self.controlMaxHeight())
            control_height = max(control_height, self.controlMinHeight())

        label_width = max(0, self.width() - control_width - spacing - self.__margins[0] - self.__margins[2])

        if self.__anchor == BaseControl.AnchorRight:
            if self.__label_place == BaseControl.LabelRight:
                label_width = min(text_width, label_width)
                label_pos_x = self.width() - self.__margins[2] - label_width
                control_pos_x = label_pos_x - spacing - control_width
            else:
                label_pos_x = self.__margins[0]
                control_pos_x = self.width() - self.__margins[2] - control_width
        else:
            if self.__label_place == BaseControl.LabelRight:
                label_pos_x = self.__margins[0] + control_width + spacing
                control_pos_x = self.__margins[0]
            else:
                label_width = min(text_width, label_width)
                label_pos_x = self.__margins[0]
                control_pos_x = self.__margins[0] + label_width + spacing

        self._label.setFixedWidth(label_width)
        self._label.move(label_pos_x, (self.height() - self._label.height()) / 2)
        self._control.setFixedSize(control_width, control_height)
        self._control.move(control_pos_x, (self.height() - control_height) / 2)

    # query methods
    def label(self):
        return self._label

    def control(self):
        return self._control

    def spacing(self):
        return self.__spacing

    def margins(self):
        return self.__margins

    def text(self):
        return self._label.text()

    def textAlignment(self):
        return self._label.alignment()

    def textElision(self):
        return self._label.elision()

    def textFont(self):
        return self._label.font()

    def controlSizePolicy(self):
        return self._control.sizePolicy()

    def controlMinWidth(self):
        return self.__control_width_range[0] or 0

    def controlMaxWidth(self):
        return self.__control_width_range[1]

    def controlMinHeight(self):
        return self.__control_height_range[0] or 0

    def controlMaxHeight(self):
        return self.__control_height_range[1]

    def contentVisible(self):
        return self.__content_visible

    # setting methods
    def createControl(self):
        raise RuntimeError("Method of abstract method must be implemented!")

    def setAnchor(self, anchor):
        self.__anchor = anchor

    def setLabelPlace(self, place):
        self.__label_place = place

    def setSpacing(self, spacing):
        self.__spacing = spacing

    def setMargins(self, left, top, right, bottom):
        self.__margins = (left, top, right, bottom)

    def setText(self, text):
        self._label.setText(text)

    def setTextAlignment(self, alignment):
        self._label.setAlignment(alignment)

    def setTextElision(self, elision):
        self._label.setElision(elision)

    def setTextFont(self, font):
        self._label.setFont(font)

    def setLabelWidthConstraint(self, value):
        self.__label_width_constraint = value

    def setControlSizePolicy(self, h_policy, v_policy):
        self._control.setSizePolicy(
            QtWidgets.QSizePolicy(h_policy, v_policy)
        )

    def setControlMinWidth(self, value):
        if value < 0:
            raise ValueError("Positive value required!")
        self.__control_width_range = (value, self.__control_width_range[1])

    def setControlMaxWidth(self, value):
        if value < 0:
            raise ValueError("Positive value required!")
        self.__control_width_range = (self.__control_width_range[0], value)

    def setControlMinHeight(self, value):
        if value < 0:
            raise ValueError("Positive value required!")
        self.__control_height_range = (value, self.__control_height_range[1])

    def setControlMaxHeight(self, value):
        if value < 0:
            raise ValueError("Positive value required!")
        self.__control_height_range = (self.__control_height_range[0], value)

    def setContentVisible(self, value):
        self._label.setVisible(value)
        self._control.setVisible(value)
        self.__content_visible = value


class ControlCheckBox(BaseControl):
    def __init__(self, label=None, parent=None):
        super(ControlCheckBox, self).__init__(label, parent)

        self.setControlSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)

        self.setAnchor(BaseControl.AnchorLeft)
        self.setLabelPlace(BaseControl.LabelRight)
        self.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        self.setTextElision(QtCore.Qt.ElideRight)

    # query methods
    def value(self):
        return self._control.isChecked()

    # setting methods
    def createControl(self):
        return QtWidgets.QCheckBox()

    def setValue(self, value):
        self._control.setChecked(value)


class ControlSpinBox(BaseControl):
    def __init__(self, label=None, parent=None):
        super(ControlSpinBox, self).__init__(label, parent)

        self.setControlSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

        self.setAnchor(BaseControl.AnchorLeft)
        self.setLabelPlace(BaseControl.LabelLeft)
        self.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
        self.setTextElision(QtCore.Qt.ElideRight)

    # setting methods
    def createControl(self):
        return QtWidgets.QSpinBox()


class ControlComboBox(BaseControl):
    def __init__(self, label=None, parent=None):
        super(ControlComboBox, self).__init__(label, parent)

        self.setControlSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

        self.setAnchor(BaseControl.AnchorLeft)
        self.setLabelPlace(BaseControl.LabelLeft)
        self.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight)
        self.setTextElision(QtCore.Qt.ElideLeft)

    # setting methods
    def createControl(self):
        control = QtWidgets.QComboBox()
        control.addItems(["one", "two", "three"])
        return control


class ControlExpander(BaseControl):
    def __init__(self, label=None, parent=None):
        super(ControlExpander, self).__init__(label, parent)

        self.setControlSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)

        self.setAnchor(BaseControl.AnchorLeft)
        self.setLabelPlace(BaseControl.LabelRight)
        self.setTextAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft)
        self.setTextElision(QtCore.Qt.ElideRight)

    # query methods
    def expanderValue(self):
        return self._control.expander.isChecked()

    def checkerValue(self):
        return self._control.checker.isChecked()

    # setting methods
    def createControl(self):
        frame = QtWidgets.QWidget()
        frame.expander = ArrowCheckBox()
        frame.checker = QtWidgets.QCheckBox()
        layout = QtWidgets.QHBoxLayout(frame)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(frame.expander)
        layout.addWidget(frame.checker)
        return frame

    def setExpanderVisible(self, value):
        self._control.expander.setRenderable(value)

    def setExpanderValue(self, value):
        self._control.expander.setChecked(value)

    def setCheckerValue(self, value):
        self._control.checker.setChecked(value)


class TabControl(QtWidgets.QTabBar):
    def __init__(self, parent=None):
        super(TabControl, self).__init__(parent)

        self.__icons = dict()

        self.__t_width = None
        self.__t_height = None
        self.__i_width = None
        self.__b_width = 2

        self.set_tab_size(75, 75)

    # events
    def paintEvent(self, event):
        palette = self.palette()

        qp = QtWidgets.QStylePainter(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing, True)

        option = QtWidgets.QStyleOptionTab()

        for index in range(self.count()):
            self.initStyleOption(option, index)
            color_group = get_color_group(option)
            bg_darker = 100
            bg_lighter = 100
            bg_color = palette.color(color_group, QtGui.QPalette.ColorRole.Midlight)
            # define hover
            if QtWidgets.QStyle.State_MouseOver & option.state:
                bg_lighter += 10
            # define pressed
            if QtWidgets.QStyle.State_Sunken & option.state:
                bg_darker += 20
            # apply color corrections
            if bg_darker > 100:
                bg_color = bg_color.darker(bg_darker)
            if bg_lighter > 100:
                bg_color = bg_color.lighter(bg_lighter)

            if self.currentIndex() == index:
                qp.fillRect(QtCore.QRect(option.rect), bg_color.darker(120))
                rect = QtCore.QRect(option.rect)
                rect.setLeft(rect.width() - self.__i_width)
                qp.fillRect(rect, QtGui.QColor(173, 216, 230))
            else:
                qp.fillRect(QtCore.QRect(option.rect), bg_color)

            rect = QtCore.QRect(option.rect)
            rect.setTop(rect.top() + self.__t_height - self.__b_width)
            qp.fillRect(rect, QtGui.QColor(36, 36, 36))

            pm = self.__icons.get(index)
            if pm:
                icon_width = min(self.__t_width, pm.width())
                icon_height = min(self.__t_height, pm.height())
                rect = QtCore.QRect(
                    option.rect.x() + (self.__t_width - icon_width) / 2,
                    option.rect.y() + (self.__t_height - icon_height) / 2,
                    icon_width,
                    icon_height
                )
                qp.drawPixmap(rect, pm)

    # setting methods
    def set_tab_size(self, width, height):
        self.__t_width = width
        self.__t_height = height
        self.__i_width = int(width * 0.1)
        self.setStyleSheet("QTabBar::tab {{ height: {}px; width: {}px;}}".format(width, height))

    def set_tab_pixmap(self, index, pixmap):
        self.__icons[index] = pixmap


class TabPane(QtWidgets.QTabWidget):
    def __init__(self, parent=None):
        super(TabPane, self).__init__(parent)

        self.tabControl = TabControl(self)
        self.setTabBar(self.tabControl)
        self.setTabPosition(QtWidgets.QTabWidget.West)

    def paintEvent(self, event):
        option = QtWidgets.QStyleOptionTabWidgetFrame()
        self.initStyleOption(option)
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing, False)
        palette = self.palette()
        c_width = self.tabControl.width()
        qp.fillRect(
            QtCore.QRect(0, 0, c_width, self.height()),
            palette.color(QtGui.QPalette.ColorRole.Midlight)
        )
        rect = QtCore.QRect(c_width, 0, self.width() - c_width, self.height())
        qp.fillRect(
            rect,
            palette.color(QtGui.QPalette.ColorRole.Window)
        )
        pen = QtGui.QPen()
        pen.setColor(palette.color(QtGui.QPalette.ColorRole.Base))
        pen.setWidth(2)
        qp.setPen(pen)
        qp.drawRect(rect)
        qp.end()

    def add_icon_tab(self, widget, icon_path):
        index = self.count()
        self.addTab(widget, "")
        pm = QtGui.QPixmap(icon_path)
        self.tabControl.set_tab_pixmap(index, pm)


class TableContainer(QtWidgets.QScrollArea):
    # toDO
    cell_widths = [220, 150]
    default_width = 150

    def __init__(self, parent=None):
        super(TableContainer, self).__init__(parent)

        self.__widgets = list()
        self.__cells = -1

        self.setWidgetResizable(True)
        # self.setFocusPolicy(QtCore.Qt.NoFocus)
        self.setFrameShadow(QtWidgets.QFrame.Plain)

        # Widget
        inner = InnerFrame(self)
        self._layout = QtWidgets.QVBoxLayout()
        self._layout.setContentsMargins(2, 2, 2, 2)
        self._layout.setSpacing(6)
        self._layout.addStretch()
        inner.setLayout(self._layout)
        self.setWidget(inner)

    # private methods
    def __widget_added_handle(self, widget):
        source_splitter = widget.widget(0) if isinstance(widget, BaseExpandedContainer) else widget
        count = source_splitter.count()
        if count > self.__cells:
            self.__cells = count

        source_splitter.state_changed.connect(self.__state_changed_handle)

        # toDO
        difference = count - len(TableContainer.cell_widths) - 1
        if difference > 0:
            TableContainer.cell_widths += [TableContainer.default_width] * difference

        for container in self.widgets():
            for widget in container.widgets():
                splitter = widget.widget(0) if isinstance(widget, BaseExpandedContainer) else widget
                difference = self.__cells - splitter.count()
                if difference > 0:
                    for index in range(difference):
                        splitter.addWidget(BaseSplitterSpacer())
                splitter.setSizes(TableContainer.cell_widths)

    def __state_changed_handle(self, widget, *args, **kwargs):
        target_sizes = widget.sizes()
        for container in self.widgets():
            for target_widget in container.widgets():
                splitter = target_widget.widget(0) if isinstance(target_widget, BaseExpandedContainer) else target_widget
                if splitter != widget:
                    splitter.setSizes(target_sizes)

        # toDO
        if TableContainer.cell_widths is None or len(TableContainer.cell_widths) <= len(target_sizes):
            TableContainer.cell_widths = target_sizes
        else:
            TableContainer.cell_widths = target_sizes + TableContainer.cell_widths[len(target_sizes):]

    def widgets(self):
        return self.__widgets

    def add(self, name, annotation="", family_settings=False):
        widget = FamilyContainer()

        self.__widgets.append(widget)
        index = self._layout.count() - 1
        self._layout.insertWidget(index, widget)

        widget.widget_added.connect(self.__widget_added_handle)

        widget.add(name, annotation)
        widget.setFamilySettings(family_settings, force=True)
        widget.setExpanded(True, force=True)

        return widget


class BaseSlider(QtWidgets.QSlider):
    value_changed = QtCore.Signal(float)
    precision_changed = QtCore.Signal(float)

    def __init__(self, orientation=QtCore.Qt.Horizontal, precision=2, parent=None):
        super(BaseSlider, self).__init__(orientation, parent)

        self.__factor = 1
        self.set_precision(precision)

        self.valueChanged.connect(self.__value_changed_handle)

    # private methods
    def __value_changed_handle(self, value):
        self.value_changed.emit(float(value) / self.__factor)

    # query methods
    def minimum(self):
        return float(super(BaseSlider, self).minimum()) / self.__factor

    def maximum(self):
        return float(super(BaseSlider, self).maximum()) / self.__factor

    def value(self):
        return float(super(BaseSlider, self).value()) / self.__factor

    # setting methods
    def set_precision(self, value):
        if not (isinstance(value, int) and value >= 0):
            raise ValueError("Positive int required!")
        __min = self.minimum()
        __max = self.maximum()
        __value = self.value()
        self.__factor = 10 ** value
        self.setMinimum(__min)
        self.setMaximum(__max)
        self.setValue(__value)
        self.precision_changed.emit(value)

    def setMinimum(self, value):
        super(BaseSlider, self).setMinimum(int(self.__factor * value))

    def setMaximum(self, value):
        super(BaseSlider, self).setMaximum(int(self.__factor * value))

    def setValue(self, value):
        super(BaseSlider, self).setValue(int(self.__factor * value))


class FieldSlider(QtWidgets.QWidget):
    value_changed = QtCore.Signal(float)

    def __init__(self, precision=2, parent=None):
        super(FieldSlider, self).__init__(parent)

        self.__slider = BaseSlider(QtCore.Qt.Horizontal)
        self.__spinbox = QtWidgets.QDoubleSpinBox()
        self.__spinbox.setSingleStep(0.1)

        self.set_precision(precision)

        layout = QtWidgets.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__slider)
        layout.addWidget(self.__spinbox)

        self.__slider.value_changed.connect(self.__slider_value_changed_handle)
        self.__spinbox.valueChanged.connect(self.__spinbox_value_changed_handle)

    # private methods
    def __slider_value_changed_handle(self, value):
        self.__spinbox.setValue(value)
        self.value_changed.emit(value)

    def __spinbox_value_changed_handle(self, value):
        self.__slider.setValue(value)
        self.value_changed.emit(value)

    # query methods
    def slider(self):
        return self.__slider

    def spinbox(self):
        return self.__spinbox

    def value(self):
        return self.__spinbox.value()

    # setting methods
    def setMinimum(self, value):
        self.__slider.setMinimum(value)
        self.__spinbox.setMinimum(value)

    def setMaximum(self, value):
        self.__slider.setMaximum(value)
        self.__spinbox.setMaximum(value)

    def setValue(self, value):
        self.__slider.setValue(value)
        self.__spinbox.setValue(value)

    def set_precision(self, precision):
        self.__slider.set_precision(precision)
        self.__spinbox.setDecimals(precision)


class PreviewItem(QtWidgets.QFrame):
    button_released = QtCore.Signal(object, object)

    def __init__(self, text, parent=None):
        super(PreviewItem, self).__init__(parent)

        self.__selected = False
        self.__hovered = False

        self.__align = QtCore.Qt.AlignVCenter | QtCore.Qt.AlignRight
        self.__elide = QtCore.Qt.ElideRight
        self.__margin = 4
        self.__text = ""

        self.setText(text)
        font = self.font()
        font.setWeight(QtGui.QFont.Bold)
        self.setFont(font)

    # events
    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.fillRect(self.rect(), QtGui.QColor(36, 36, 36))

        label_height = int(self.height() * 0.2)

        point_size = int(round(label_height / 3.0))
        if point_size > 2:
            label_rect = QtCore.QRect(0, self.height() - label_height, self.width(), label_height)
            qp.fillRect(label_rect, QtGui.QColor(128, 128, 200, 200))

            font = self.font()
            font.setPointSize(point_size)
            self.setFont(font)

            fm = QtGui.QFontMetrics(self.font())
            pen = QtGui.QPen()
            pen.setColor(QtGui.QColor(240, 240, 240))
            qp.setPen(pen)

            qp.drawText(
                QtCore.QRect(
                    self.__margin,
                    label_rect.y(),
                    self.width() - 2 * self.__margin,
                    label_rect.height()
                ),
                self.__align,
                fm.elidedText(self.text(), self.__elide, self.width())
            )
        if self.__selected:
            pen = QtGui.QPen()
            pen.setColor(QtGui.QColor(50, 200, 0, 140))
            pen.setWidth(6)
            qp.setPen(pen)
            qp.drawRect(self.rect())
        if self.__hovered:
            qp.fillRect(self.rect(), QtGui.QColor(128, 128, 255, 16))
        qp.end()

    def mouseReleaseEvent(self, event):
        self.button_released.emit(self, event)

    def enterEvent(self, event):
        if not self.__hovered:
            self.__hovered = True
            self.update()

    def leaveEvent(self, event):
        if self.__hovered:
            self.__hovered = False
            self.update()

    # query methods
    def text(self):
        return self.__text

    def selected(self):
        return self.__selected

    # setting methods
    def setText(self, text):
        self.__text = str(text)

    def setSelected(self, selected):
        if selected != self.__selected:
            self.__selected = bool(selected)
            self.update()


class ProgressBar(QtWidgets.QProgressBar):
    finished = QtCore.Signal()

    def __init__(self, parent=None):
        super(ProgressBar, self).__init__(parent)

        self.setStyleSheet(
            "QProgressBar {"
            "color: rgb(220, 220, 220);"
            "background-color: rgb(36, 36, 36);"
            "border: 1px solid rgb(220, 220, 220);"
            "}"
            "QProgressBar::chunk {"
            "background-color: rgb(32, 103, 136);"
            "}"
        )
        self.setAlignment(QtCore.Qt.AlignCenter)

        self.valueChanged.connect(self.__value_changed_handle)

    # private methods
    def __value_changed_handle(self, *args):
        if self.value() >= self.maximum():
            self.finished.emit()

    # setting methods
    def setText(self, text):
        self.setFormat("{}: %p%".format(text) if text else "%p%")
