from window.ui import widgets
from vendor.Qt import QtCore, QtGui, QtWidgets


class HelpPane(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(HelpPane, self).__init__(parent)

        self.__messages_bar = widgets.ElidedLabel()
        self.__messages_bar.setSizePolicy(
            QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        )
        self.__progress_bar = widgets.ProgressBar()
        self.__progress_bar.setFixedHeight(20)

        layout = QtWidgets.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__messages_bar)
        layout.addWidget(self.__progress_bar)

        self.setFixedHeight(22)

        self.__progress_bar.finished.connect(self.__progress_finished_handle)

        self.reset()

    # private methods
    def __progress_finished_handle(self):
        self.__progress_bar.hide()

    # events
    def mousePressEvent(self, event):
        self.test()

    # query methods
    def messagesBar(self):
        return self.__messages_bar

    def progressBar(self):
        return self.__progress_bar

    # setting methods
    def reset(self):
        self.__messages_bar.setText("click here...")
        self.__progress_bar.setRange(0, 0)
        self.__progress_bar.setValue(0)
        self.__progress_bar.setText("")

    def test(self):
        import time
        import random
        self.__messages_bar.show()
        self.__progress_bar.show()
        self.__messages_bar.setText(
            "{}: {} {} {}...".format(
                random.choice(["Execute", "Validate", "Collect"]),
                random.randint(2, 50),
                random.choice(["objects", "nodes", "items", "elements"]),
                random.choice(["found", "deleted", "created", "renamed"])
            )
        )
        self.__progress_bar.setText(
            "{} {}".format(
                random.choice(["Collecting", "Validating", "Deleting"]),
                random.choice(["cameras", "groups", "geometry", "cache", "lights"])
            )
        )
        max_value = random.randint(5, 15)
        self.__progress_bar.setRange(0, max_value)
        for i in range(0, max_value + 1):
            self.__progress_bar.setValue(i)
            time.sleep(0.05 + 0.02 * random.randint(1, 5))
