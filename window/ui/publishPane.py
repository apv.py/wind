from window.ui import widgets
from vendor.Qt import QtCore, QtGui, QtWidgets


class PublishPane(widgets.BaseWidget):
    def __init__(self, parent=None):
        super(PublishPane, self).__init__(parent)

        self.tableContainer = widgets.TableContainer()
        self.notePane = NotePane()
        self.controlPane = PublishControl()

        layout = QtWidgets.QVBoxLayout(self)
        layout.addWidget(self.tableContainer)
        layout.addWidget(self.notePane)
        layout.addWidget(self.controlPane)


class NotePane(widgets.BaseWidget):
    def __init__(self, parent=None):
        super(NotePane, self).__init__(parent)

        self.label = QtWidgets.QLabel("Note:")
        self.textArea = QtWidgets.QTextEdit()

        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(1, 4, 1, 1)
        layout.addWidget(self.label)
        layout.addWidget(self.textArea)
        self.setLayout(layout)

        self.setFixedHeight(120)

    def get_text(self):
        return self.textArea.toPlainText()

    def clear(self):
        self.textArea.setText("")


class PublishControl(widgets.BaseWidget):
    def __init__(self, parent=None):
        super(PublishControl, self).__init__(parent)

        self.resetButton = widgets.BasePushButton("Reset")
        self.resetButton.setFixedWidth(80)
        self.executeButton = widgets.BasePushButton("Execute")

        layout = QtWidgets.QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.resetButton)
        layout.addWidget(self.executeButton)
