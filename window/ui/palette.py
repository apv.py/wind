# noinspection PyUnresolvedReferences
from vendor.Qt import QtCore, QtGui, QtWidgets


definition = {
    "AlternateBase": {
        "Active": (46, 46, 46, 255),
        "Disabled": (46, 46, 46, 255),
        "Inactive": (46, 46, 46, 255)
    },
    "Background": {
        "Active": (68, 68, 68, 255),
        "Disabled": (68, 68, 68, 255),
        "Inactive": (68, 68, 68, 255)
    },
    "Base": {
        "Active": (43, 43, 43, 255),
        "Disabled": (43, 43, 43, 255),
        "Inactive": (43, 43, 43, 255)
    },
    "BrightText": {
        "Active": (37, 37, 37, 255),
        "Disabled": (255, 255, 255, 255),
        "Inactive": (37, 37, 37, 255)
    },
    "Button": {
        "Active": (93, 93, 93, 255),
        "Disabled": (75, 75, 75, 255),
        "Inactive": (93, 93, 93, 255)
    },
    "ButtonText": {
        "Active": (238, 238, 238, 255),
        "Disabled": (128, 128, 128, 255),
        "Inactive": (238, 238, 238, 255)
    },
    "Dark": {
        "Active": (37, 37, 37, 255),
        "Disabled": (37, 37, 37, 255),
        "Inactive": (37, 37, 37, 255)
    },
    "Foreground": {
        "Active": (187, 187, 187, 255),
        "Disabled": (128, 128, 128, 255),
        "Inactive": (187, 187, 187, 255)
    },
    "Highlight": {
        "Active": (82, 133, 166, 255),
        "Disabled": (82, 102, 116, 255),
        "Inactive": (82, 133, 166, 255)
    },
    "HighlightedText": {
        "Active": (255, 255, 255, 255),
        "Disabled": (255, 255, 255, 255),
        "Inactive": (255, 255, 255, 255)
    },
    "Light": {
        "Active": (97, 97, 97, 255),
        "Disabled": (97, 97, 97, 255),
        "Inactive": (97, 97, 97, 255)
    },
    "Link": {
        "Active": (0, 0, 238, 255),
        "Disabled": (0, 0, 238, 255),
        "Inactive": (0, 0, 238, 255)
    },
    "LinkVisited": {
        "Active": (82, 24, 139, 255),
        "Disabled": (82, 24, 139, 255),
        "Inactive": (82, 24, 139, 255)
    },
    "Midlight": {
        "Active": (55, 55, 55, 255),
        "Disabled": (55, 55, 55, 255),
        "Inactive": (55, 55, 55, 255)
    },
    "NoRole": {
        "Active": (0, 0, 0, 255),
        "Disabled": (0, 0, 0, 255),
        "Inactive": (0, 0, 0, 255)
    },
    "Shadow": {
        "Active": (0, 0, 0, 255),
        "Disabled": (0, 0, 0, 255),
        "Inactive": (0, 0, 0, 255)
    },
    "Text": {
        "Active": (200, 200, 200, 255),
        "Disabled": (105, 105, 105, 255),
        "Inactive": (200, 200, 200, 255)
    },
    "ToolTipBase": {
        "Active": (255, 255, 220, 255),
        "Disabled": (255, 255, 220, 255),
        "Inactive": (255, 255, 220, 255)
    },
    "ToolTipText": {
        "Active": (0, 0, 0, 255),
        "Disabled": (0, 0, 0, 255),
        "Inactive": (0, 0, 0, 255)
    },
    "Window": {
        "Active": (68, 68, 68, 255),
        "Disabled": (68, 68, 68, 255),
        "Inactive": (68, 68, 68, 255)
    },
    "WindowText": {
        "Active": (187, 187, 187, 255),
        "Disabled": (128, 128, 128, 255),
        "Inactive": (187, 187, 187, 255)
    }
}
