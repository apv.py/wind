# noinspection PyUnresolvedReferences
from vendor.Qt import QtCore, QtGui, QtWidgets
# noinspection PyUnresolvedReferences
from window.ui import widgets, browserPane, publishPane, helpPane
import window


class Window(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        self.setWindowTitle("Wind")
        self.setMinimumSize(QtCore.QSize(960, 600))

        widget = widgets.BaseWidget(self)
        #
        layout = QtWidgets.QVBoxLayout(widget)
        layout.setContentsMargins(2, 2, 2, 2)
        layout.setSpacing(2)

        self.tabPane = widgets.TabPane(widget)
        self.tabPane.add_icon_tab(
            browserPane.BrowserPane(), window.ROOT + "/icons/tab_browser.png")
        self.tabPane.add_icon_tab(
            publishPane.PublishPane(), window.ROOT + "/icons/tab_publish.png")
        self.helpPane = helpPane.HelpPane()

        self.setCentralWidget(widget)
        layout.addWidget(self.tabPane)
        layout.addWidget(widgets.HorizontalLine())
        layout.addWidget(self.helpPane)

        # TEMP
        #    PUBLISH PANE
        family_1 = self.tabPane.widget(1).tableContainer.add("scene", "publish scene", True)
        for name in ("scene", "proxy"):
            process_widget = family_1.add(name)
            process_widget.add(widgets.Status.Info, "one")
            process_widget.add(widgets.Status.Error, "two")
            process_widget.add(widgets.Status.Warning, "done")
        family_2 = self.tabPane.widget(1).tableContainer.add("alembic", "export alembic", False)
        for name in ("ASST01_001", "ASST01_002", "ASST01_003"):
            process_widget = family_2.add(name)
            process_widget.add(widgets.Status.Info, "path: c:/temp" * 16)
            process_widget.add(widgets.Status.Warning, "done")

