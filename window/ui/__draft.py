# noinspection PyUnresolvedReferences
from vendor.Qt import QtCore, QtGui, QtWidgets


# # private methods
# def __set_signals(self):
#     signals = (
#         ("pressed", ()),
#         ("released", ()),
#         ("stateChanged", (int,)),
#         ("toggled", (bool,)),
#     )
#
#     for name, args in signals:
#         if hasattr(self._control, name):
#             signal = QtCore.Signal(*args)
#             setattr(BaseControl, name, signal)
#             getattr(self._control, name).connect(getattr(self, name).emit)


# def connectLabel(self, signal_name, slot):
#     if hasattr(self._label, signal_name):
#         getattr(self._label, signal_name).connect(slot)
#     else:
#         raise AttributeError("Label has no signal \"{}\"!".format(signal_name))
#
#
# def connectControl(self, signal_name, slot):
#     if hasattr(self._control, signal_name):
#         getattr(self._control, signal_name).connect(slot)
#     else:
#         raise AttributeError("Control has no signal \"{}\"!".format(signal_name))

class ControlCheckBox(QtWidgets.QCheckBox):
    def __init__(self, *args, **kwargs):
        super(ControlCheckBox, self).__init__(*args, **kwargs)

        # UI settings
        self.__anchor_right = False
        self.__label_right = True
        self.__text_align = QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft
        self.__text_elide = QtCore.Qt.ElideRight
        self.__spacing = 6
        self.__margins = (2, 2)

        # control data
        self.__buttons = QtCore.Qt.NoButton
        self.__control_rect = None
        self.__control_under_mouse = None
        self.__control_pressed = None

    def mouseMoveEvent(self, event):
        value = self.__control_rect and self.__control_rect.contains(event.pos())
        if value != self.__control_under_mouse:
            self.__control_under_mouse = value
            self.update()

    def mousePressEvent(self, event):
        self.__buttons = event.buttons()
        if event.button() == QtCore.Qt.LeftButton:
            self.__control_pressed = self.__control_under_mouse
        super(ControlCheckBox, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        self.__buttons = event.buttons()
        if self.__control_pressed:
            if event.button() == QtCore.Qt.LeftButton:
                if self.__control_under_mouse:
                    self.toggle()
                self.__control_pressed = False
        self.clearFocus()

    def paintEvent(self, event):
        qp = QtWidgets.QStylePainter(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing, True)

        option = QtWidgets.QStyleOptionButton()
        if self.isActiveWindow():
            option.state |= QtWidgets.QStyle.State_Active
        if self.isEnabled():
            option.state |= QtWidgets.QStyle.State_Enabled
        if self.isChecked():
            option.state |= QtWidgets.QStyle.State_On
        else:
            option.state |= QtWidgets.QStyle.State_Off
        if self.__control_under_mouse:
            if self.__control_pressed:
                option.state |= QtWidgets.QStyle.State_Sunken
            else:
                option.state |= QtWidgets.QStyle.State_MouseOver

        qs = QtWidgets.QApplication.style()
        control_width = qs.pixelMetric(QtWidgets.QStyle.PM_IndicatorWidth, option)
        control_height = qs.pixelMetric(QtWidgets.QStyle.PM_IndicatorHeight, option)

        spacing = self.__spacing if self.text() else 0

        fm = QtGui.QFontMetrics(self.font())
        label_width = self.width() - control_width - spacing - sum(self.__margins)
        label_height = fm.height()
        if self.__anchor_right:
            if self.__label_right:
                label_width = min(fm.width(self.text()), label_width)
                label_pos_x = self.width() - self.__margins[1] - label_width
                control_pos_x = label_pos_x - control_width - spacing
            else:
                label_pos_x = self.__margins[0]
                control_pos_x = self.width() - self.__margins[1] - control_width
        else:
            if self.__label_right:
                label_pos_x = self.__margins[0] + control_width + spacing
                control_pos_x = self.__margins[0]
            else:
                label_width = min(fm.width(self.text()), label_width)
                label_pos_x = self.__margins[0]
                control_pos_x = self.__margins[0] + label_width + spacing

        label_rect = QtCore.QRect(
            label_pos_x,
            (self.height() - label_height) / 2,
            label_width,
            label_height
        )
        self.__control_rect = QtCore.QRect(
            control_pos_x,
            (self.height() - control_height) / 2,
            control_width,
            control_height
        )
        qp.drawText(
            label_rect,
            self.__text_align,
            fm.elidedText(self.text(), self.__text_elide, label_width)
        )
        option.rect = self.__control_rect
        qp.drawControl(QtWidgets.QStyle.CE_CheckBox, option)


class ControlSpinBoxLineEdit(QtWidgets.QLineEdit):
    def __init__(self, *args, **kwargs):
        super(ControlSpinBoxLineEdit, self).__init__(*args, **kwargs)

    def paintEvent(self, event):
        pass
        # print("event")


class ControlSpinBox(QtWidgets.QAbstractSpinBox):
    def __init__(self, text, *args, **kwargs):
        super(ControlSpinBox, self).__init__(*args, **kwargs)

        self.__text = text

        self.__line_edit = ControlSpinBoxLineEdit()
        self.setLineEdit(self.__line_edit)

        # UI settings
        self.__anchor_right = False
        self.__label_right = True
        self.__text_align = QtCore.Qt.AlignVCenter | QtCore.Qt.AlignLeft
        self.__text_elide = QtCore.Qt.ElideRight
        self.__spacing = 6
        self.__margins = (2, 2)

        # control data
        self.__buttons = QtCore.Qt.NoButton
        self.__control_rect = None
        self.__control_under_mouse = None
        self.__control_pressed = None

        self.__up_control_rect = None
        self.__down_control_rect = None

        self.setMouseTracking(True)
        self.clearMask()
    #     self.installEventFilter(self)
    #
    #
    # def eventFilter(self, object, event):
    #     if event.type() == QtCore.QEvent.MouseMove:
    #         # self.mouseMoveEvent(event)
    #         print("!!!!!!!!!!")
    #     return True



    def mouseMoveEvent(self, event):
        print("move")
        event.accept()
        # value = self.__control_rect and self.__control_rect.contains(event.pos())
        # if value != self.__control_under_mouse:
        #     self.__control_under_mouse = value + True
        #     self.update()
    #
    # def mousePressEvent(self, event):
    #     self.__buttons = event.buttons()
    #     if event.button() == QtCore.Qt.LeftButton:
    #         self.__control_pressed = self.__control_under_mouse
    #     super(ControlSpinBox, self).mousePressEvent(event)
    #
    # def mouseReleaseEvent(self, event):
    #     self.__buttons = event.buttons()
    #     if self.__control_pressed:
    #         if event.button() == QtCore.Qt.LeftButton:
    #             if self.__control_under_mouse:
    #                 self.toggle()
    #             self.__control_pressed = False
    #     self.clearFocus()

    def paintEvent(self, event):
        qp = QtWidgets.QStylePainter(self)
        qp.setRenderHint(QtGui.QPainter.Antialiasing, True)

        qs = QtWidgets.QApplication.style()

        option = QtWidgets.QStyleOptionSpinBox()
        if self.__text == "on farm":
            option.stepEnabled |= QtWidgets.QAbstractSpinBox.StepUpEnabled
            option.stepEnabled |= QtWidgets.QAbstractSpinBox.StepDownEnabled

            option.frame = True

            if self.isActiveWindow():
                option.state |= QtWidgets.QStyle.State_Active
            if self.isEnabled():
                option.state |= QtWidgets.QStyle.State_Enabled
            if self.hasFocus():
                option.state |= QtWidgets.QStyle.State_HasFocus

            option.state |= QtWidgets.QStyle.State_MouseOver

            option.rect = QtCore.QRect(50, 0, 50, 24)

            self.__up_control_rect = qs.subControlRect(
                QtWidgets.QStyle.CC_SpinBox,
                option,
                QtWidgets.QStyle.SC_SpinBoxUp,
                self
            )
            self.__down_control_rect = qs.subControlRect(
                QtWidgets.QStyle.CC_SpinBox,
                option,
                QtWidgets.QStyle.SC_SpinBoxDown,
                self
            )
            # print(self.__up_control_rect, self.__down_control_rect, "m")

            # print(int(option.state), int(option.stepEnabled), int(option.subControls), int(option.activeSubControls), "m")

        else:
            self.initStyleOption(option)
            # print(int(option.state), int(option.stepEnabled), int(option.subControls), int(option.activeSubControls))

            r = qs.subControlRect(
                QtWidgets.QStyle.CC_SpinBox,
                option,
                QtWidgets.QStyle.SC_SpinBoxUp,
                self
            )
            # print(r)



        # qs = QtWidgets.QApplication.style()
        # control_width = qs.pixelMetric(QtWidgets.QStyle.PM_IndicatorWidth, option) * 4
        # control_height = qs.pixelMetric(QtWidgets.QStyle.PM_IndicatorHeight, option) * 4
        #
        # fm = QtGui.QFontMetrics(self.font())
        # label_width = self.width() - control_width - self.__spacing - sum(self.__margins)
        # label_height = fm.height()
        # if self.__anchor_right:
        #     if self.__label_right:
        #         label_width = min(fm.width(self.text()), label_width)
        #         label_pos_x = self.width() - label_width - self.__margins[1]
        #         control_pos_x = self.width() - control_width - label_width - self.__spacing - self.__margins[1]
        #     else:
        #         label_pos_x = self.__margins[0]
        #         control_pos_x = self.width() - control_width - self.__margins[1]
        # else:
        #     if self.__label_right:
        #         label_pos_x = control_width + self.__spacing + self.__margins[0]
        #         control_pos_x = self.__margins[0]
        #     else:
        #         label_width = min(fm.width(self.text()), label_width)
        #         label_pos_x = self.__margins[0]
        #         control_pos_x = label_width + self.__spacing + self.__margins[0]
        #
        # label_rect = QtCore.QRect(
        #     label_pos_x,
        #     (self.height() - label_height) / 2,
        #     label_width,
        #     label_height
        # )
        # self.__control_rect = QtCore.QRect(
        #     control_pos_x,
        #     (self.height() - control_height) / 2,
        #     control_width,
        #     control_height
        # )
        # qp.drawText(
        #     label_rect,
        #     self.__text_align,
        #     fm.elidedText(self.text(), self.__text_elide, label_width)
        # )
        # option.rect = self.__control_rect

        # option.rect = QtCore.QRect(50, 0, 100, 50)
        qp.drawComplexControl(QtWidgets.QStyle.CC_SpinBox, option)