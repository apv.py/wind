from window.ui import window, palette, widgets
from vendor.Qt import QtCore, QtGui, QtWidgets
import sys

app = QtWidgets.QApplication(sys.argv)
app.setPalette(
    widgets.get_palette(palette.definition)
)

win = window.Window()
win.setStyleSheet(
    "QAbstractItemView, QAbstractScrollArea "
    "{"
    "border: 1px solid rgb(37, 37, 37)"
    "}"
)
win.show()

app.exec_()
sys.exit()
